package rabbitmq;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.concurrent.TimeoutException;
import java.io.File;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class Publisher {

    private final static String QUEUE_NAME = "zoom";

    public static void main(String[]args) throws IOException, TimeoutException, ParseException {
       
    	ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost"); 
        Connection connection = factory.newConnection();
        com.rabbitmq.client.Channel channel = connection.createChannel();
        
        //JSONParser jsonParser = new JSONParser();
       //FileReader reader = new FileReader("objectWithSomeExplanation.json");
        File file = new File ("C:\\Users\\Juda\\Desktop\\ProActive\\objectWithSomeExplanation.json");
    	
//        String content = readFile(file.path);
        //Object obj = jsonParser.parse(reader);
        	
//        String messege = reader;
        String message = "hello ZOom";
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
        System.out.println(" [x] Sent '" + message + "'");

        channel.close();
        connection.close();
    }
}
