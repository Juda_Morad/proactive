package drive;

import java.io.IOException;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import Base.TestBase;

public class SetUp extends TestBase
{
	@BeforeSuite
	public void SetUp() throws InterruptedException, IOException
	{
		initialization();
		login();
		Thread.sleep(5000);
	}

}
