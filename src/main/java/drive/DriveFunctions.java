package drive;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import Base.TestBase;

public class DriveFunctions extends TestBase
{
	
	DriveElements Elements;
	
	public DriveFunctions()
	{
		this.Elements = new DriveElements();
	}
	
	public void uploadFile()
	{	
		waitForVisability(Elements.uploadFilebutton);
		Elements.uploadFilebutton.click();
		WebElement upload = driver.findElement(By.cssSelector("#upload-button"));
		upload.sendKeys("‪C:\\Users\\Juda\\Pictures\\download.jpg");
	}
}