package drive;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Base.TestBase;

public class DriveElements extends TestBase
{

	
	@FindBy(id = "upload-button")
	public WebElement uploadFilebutton;

	@FindBy(css = "[aria-label='Home'] .material-icons")
	public WebElement homeButtonIcon ; 
	
	@FindBy(css = "[alt='File Browser']")
	public WebElement homeButtonImg ;
	
	@FindBy(css = "[aria-label='הקבצים שלי'].action")
	public WebElement MyFilesButton ; 
	
	@FindBy(css = "[aria-label='שותפו איתי'].action")
	public WebElement SharedWithMeButton ; 
	
	@FindBy(css = "[aria-label='תיקיה חדשה'].action")
	public WebElement NewFolderButton ;  
	
	@FindBy(css = "[aria-label='צור קשר'].action")
	public WebElement ContactUsButton ;  
	
	@FindBy(css = "[aria-label='ניצול אחסון']")
	public WebElement StorageTab ; 
	
	@FindBy(css = "[aria-label='שיתוף'] .material-icons")
	public WebElement ShareButton ; 
	
	@FindBy(css = "[aria-label='שנה שם'] .material-icons")
	public WebElement ReNameButton ; 
	
	@FindBy(id = "move-button")
	public WebElement MoveButton ;
	
	@FindBy (id = "delete-button")
	public WebElement DeleteButton ; 
	
	@FindBy(id = "switch-view-button")
	public WebElement SwitchViewButton ; 
	
	@FindBy(id = "download-button")
	public WebElement DownloadButton ; 
	
	@FindBy(css = "[aria-label='פרטים'] .material-icons")
	public WebElement InfoButton ;
	
	@FindBy(css = "[aria-label='בחירה מרובה'] .material-icons")
	public WebElement SelectMultipleButton ; 
	
	@FindBy(css = ".chevron~div") 
	public List<WebElement> PreviousFolders = new ArrayList<>();
	
	@FindBy(className = "name") 
	public List<WebElement> NumOfItem  = new ArrayList<>(); 
	
	@FindBy(css = "[data-dir='true']")
	public List<WebElement> NumOfFolders  = new ArrayList<>(); 
	
	@FindBy(css = "[data-dir='true'] .name")
	public List<WebElement> foldersName  = new ArrayList<>(); 
	
	@FindBy(css  = "#listing.mosaic")
	public WebElement SwitchViewBlock ;
	
	@FindBy(className = "quota-usage-container")
	public WebElement currentUsage ;
	
	@FindBy(className = "autocomplete-input")
	public WebElement searchInputField ;
	
	@FindBy(css = "[role='button'][aria-label='מיון לפי שם']")
	public WebElement sortByName ;
	
	@FindBy(css = ".chevron~[style='direction: ltr;']")
	public List <WebElement> listOfFolderHierarchy = new ArrayList<>() ;
	
	@FindBy(css =  "#multiple-selection.active")
	public List <WebElement> multipleSelection= new ArrayList<>() ;
	
	@FindBy(css =  "div:nth-child(5) > .item .name")
	public List <WebElement> fielsNameList= new ArrayList<>() ;
	
	@FindBy(css = "[role='button'][aria-label='מיון לפי גודל']")
	public WebElement sortBySize ;
	
	@FindBy(css = "[role='button'][aria-label='מיון לפי זמן שינוי אחרון']")
	public WebElement sortLastChange ;
	
	@FindBy(css = ".card-content p")
	public List <WebElement> rowsInFileOrFolderInformation  = new ArrayList<>() ; 
	
	@FindBy(css = ".size.rtl~.modified")
	public List <WebElement> lastFielsModifyList  = new ArrayList<>() ; 
	
	@FindBy(css = ".item.has-tooltip") 
	public WebElement shareTage ;
	
	@FindBy(css = ".speech-bubble.fade.rtl .bold-name")
	public WebElement userName ;
	
	@FindBy(css = "[data-dir='true'] .modified")
	public List <WebElement> lastFoldersModifyList  = new ArrayList<>() ;
	
	@FindBy(css = "#close~#delete-button")
	public  WebElement deleteButtonFileFromShearTab ;

	

	
	// כפתורים לאישור פעולות לדוגמא אישור מחיקה וכדומה 
	
	@FindBy(css = ".card-content [aria-label='יצירה']")
	public WebElement CreateButtonForShare ;

	@FindBy(css = ".card-action [aria-label='יצירה']")
	public WebElement CreateFolderConfirmButton ;
	
	@FindBy(css = ".card-action [aria-label='ביטול']")
	public WebElement CloseButton ;
	
	@FindBy(css = ".card-action.rtl [aria-label='סגירה']")
	public WebElement CloseButtonInSharTab ;
	
	@FindBy(css = ".card-action [aria-label='שנה שם']")
	public WebElement RenameConfirmButton ;
	
	@FindBy(css = ".card-action [aria-label='Cancel']")
	public WebElement CancelButton ;
	
	@FindBy(css = "ul.file-list [aria-label='..']")
	public WebElement navigateBackInFolders ; 
	
	@FindBy(css = ".card-action [aria-label='העבר']")
	public WebElement MoveConfirmButton ;
	
	@FindBy(css = ".card-action .button.button--flat.button--red")
	public WebElement  DeleteConfirmButton ;
	
	@FindBy(css = ".card-action [aria-label='אישור']")
	public WebElement  OkButton ; 
	
	@FindBy(css = 	".card-action [aria-label='סגירה']")
	public WebElement  closeShareButton ; 
	
	@FindBy(css = "[placeholder='חפש משתמש']")
	public WebElement searchUserFild ;
	
	@FindBy(css  = "input.input--block")
	public WebElement InputField; 

	@FindBy(css = ".file-list [tabindex='0']")
	public List<WebElement>  folderListMoveButton = new ArrayList<>();
	
	@FindBy(className = "card-title")
	public WebElement windowOfToolBar;
	
   @FindBy(className = "card-content")
	public WebElement windowOfToolBarDelete;
   
   @FindBy(className = "p code")
	public WebElement currentFolderInFolderTransfer;
   
   @FindBy(css = "select[aria-label='הרשאה']")
  	public WebElement selectPermissionButton ;  
   
   @FindBy (className = "delete-permission") 
  	public List<WebElement> deletePermissionButton = new ArrayList<>();	
   
   @FindBy (tagName = "li")
   public List<WebElement> rightClickList = new ArrayList<>();	
   
   @FindBy (className = "vue-tab")
  	public List <WebElement> shareWithMeButtonInTransferWindow = new ArrayList<>() ; 
   
   @FindBy (id = "previewer")
  	public WebElement fileViewBlock ;
   
   @FindBy (id = "close")
  	public WebElement closeFileView ;

	
	
	public DriveElements()
	{
		
		PageFactory.initElements(driver,this); 
		
	}
	

}
