package Base;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;


public class TestBase
{

	public static WebDriver driver;
	public static ExtentReports extent;
	public static ExtentHtmlReporter htmlReporter;
	public static ExtentTest logger;
	public static WebDriverWait wait;
	public static Properties prop;
	public static Actions action;

	// Initialize the main driver 
	public static void initialization() throws IOException {

		System.setProperty("webdriver.chrome.driver","chromedriver.exe");

		prop			   = new Properties();  
		FileInputStream ip = new FileInputStream("./configs/properties");
		prop.load(ip);

		driver 		 = new ChromeDriver();
		wait 		 = new WebDriverWait(driver, 30);
		extent 		 = new ExtentReports();
		htmlReporter = new ExtentHtmlReporter("./reports/reports.html");
		action 		 = new Actions(driver);
		extent.attachReporter(htmlReporter);


		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.navigate().to(prop.getProperty("DriveAppUrl"));
	}

	// Open new tab in incognito 
	public static void openIncognitoTab () {

		ChromeOptions options = new ChromeOptions();
		options.addArguments("start-maximized");
		options.addArguments("disable-popup-blocking");
		options.addArguments("incognito");
		driver = new ChromeDriver(options);
	}

	public static void login() {
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username"))) ;
		driver.findElement(By.id("username")).sendKeys(prop.getProperty("username1"));
		driver.findElement(By.id("password")).sendKeys(prop.getProperty("password1")); 
		driver.findElement(By.className("btn")).click();	

	}
	public void waitForVisability(WebElement element)
	{
		wait.until(ExpectedConditions.visibilityOf(element));
	}

}

		
	


class beforeWeStart extends TestBase {
	@BeforeSuite
	public void SetUp() throws InterruptedException, IOException {

		initialization();	

	}

}
